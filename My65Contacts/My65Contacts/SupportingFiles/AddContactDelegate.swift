//
//  AddContactDelegate.swift
//  My65Contacts
//
//  Created by Sergey Golovin on 19.09.2021.
//  Copyright © 2021 GoldenWindGames LLC. All rights reserved.
//

import Foundation

protocol AddContactDelegate {
    func addNewContact(contact: Contact)
    func editContact(contact: Contact, index: IndexPath)
    func deleteContact(at index: IndexPath)
}
