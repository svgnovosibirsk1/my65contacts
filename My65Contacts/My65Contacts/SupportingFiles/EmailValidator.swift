//
//  EmailValidator.swift
//  My65Contacts
//
//  Created by Sergey Golovin on 21.09.2021.
//  Copyright © 2021 GoldenWindGames LLC. All rights reserved.
//

import Foundation

class EmailValidator {
    static let shared = EmailValidator()
    
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPredicate.evaluate(with: email)
    }
}
