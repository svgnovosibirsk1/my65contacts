//
//  FavouriteViewController.swift
//  My65Contacts
//
//  Created by Sergey Golovin on 21.09.2021.
//  Copyright © 2021 GoldenWindGames LLC. All rights reserved.
//

import UIKit
import CoreData

class FavouriteViewController: UITableViewController {
    
    let cellID = "favouriteCell"
    let context = (UIApplication.shared.delegate as!AppDelegate).persistentContainer.viewContext
    
    var favouriteContacts = [Contact]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadContacts()
    }
    
    @IBAction func toAllButtonPressed(_ sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Tableview Datasource Methods
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favouriteContacts.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
        
        if let lastName = favouriteContacts[indexPath.row].surname {
            cell.textLabel?.text = favouriteContacts[indexPath.row].name! + " " + lastName
        } else {
            cell.textLabel?.text = favouriteContacts[indexPath.row].name
        }
        
        cell.detailTextLabel?.text = favouriteContacts[indexPath.row].phone
        cell.imageView?.image = UIImage(systemName: "star.fill")

        return cell
    }
    
    // MARK: - Tableview Delegate Methods
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: - CoreData methods

    func loadContacts() {
        let request: NSFetchRequest<Contact> = Contact.fetchRequest()
        request.predicate = NSPredicate(format: "isFavourite == true")
        request.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]

        do {
            favouriteContacts = try context.fetch(request)
        } catch {
            print("Fetching data from context error \(error)")
        }
        
        tableView.reloadData()
    }
}
