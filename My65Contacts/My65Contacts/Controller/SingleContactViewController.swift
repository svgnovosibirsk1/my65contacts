//
//  SingleContactViewController.swift
//  My65Contacts
//
//  Created by Sergey Golovin on 18.09.2021.
//  Copyright © 2021 GoldenWindGames LLC. All rights reserved.
//

import UIKit
import CoreData

class SingleContactViewController: UIViewController {
    var singleContactVCContact: Contact?
    var delegate: AddContactDelegate?
    var index: IndexPath?
    var isEditMode = false
    
    let context = (UIApplication.shared.delegate as!AppDelegate).persistentContainer.viewContext
    
    @IBOutlet weak var surnameTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var birdthDateTextField: UITextField!
    @IBOutlet weak var companyTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var isFavouriteSwitch: UISwitch!
    @IBOutlet weak var deleteButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.topItem?.title = ""
        
        setDismissKeyboardGesture()
        setupDeleteButton()
        fillTextFields()
        
        phoneTextField.delegate = self
        phoneTextField.keyboardType = .numberPad
    }
    
    @IBAction func okButtonPressed(_ sender: UIButton) {
        if nameTextField.text != "" {
            if emailTextField.text! != "" && !EmailValidator.shared.isValidEmail(emailTextField.text!) {
                let ac = UIAlertController(title: "Invalid e-mail", message: "Enter correct email", preferredStyle: .alert)
                ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                present(ac, animated: true)
            } else {
                setUpContact()
                navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @IBAction func deleteButtonPressed(_ sender: UIButton) {
        guard let delegate = delegate, let index = index else { return }
        delegate.deleteContact(at: index)
        navigationController?.popViewController(animated: true)
    }
    
    private func setupDeleteButton() {
        deleteButton.isEnabled = index != nil
        deleteButton.isHidden = index == nil
    }
    
    // MARK: - Create new contact
    
    private func setUpContact() {
        var contact: Contact? = nil
        
        if singleContactVCContact != nil {
            contact = singleContactVCContact!
        } else {
            contact = Contact(context: context)
        }

        contact?.surname = surnameTextField.text ?? ""
        contact?.name = nameTextField.text ?? "New contact"
        contact?.dateOfBirdth = birdthDateTextField.text ?? ""
        contact?.company = companyTextField.text ?? ""
        contact?.email = emailTextField.text ?? ""
        contact?.phone = phoneTextField.text ?? ""
        contact?.isFavourite = isFavouriteSwitch.isOn
        
        saveContacts()

        if let delegate = delegate {
            if isEditMode {
                if let index = index {
                    delegate.editContact(contact: contact!, index: index)
                }
            } else {
                delegate.addNewContact(contact: contact!)
            }
        }
    }
    
    // MARK: - CoreData save
    
    func saveContacts() {
        do {
            try context.save()
        } catch {
            print("Saving context error \(error)")
        }
    }
    
    // MARK: - Fill fields from existing contact
    
    private func fillTextFields() {
        surnameTextField.text = singleContactVCContact?.surname
        nameTextField.text = singleContactVCContact?.name
        birdthDateTextField.text = singleContactVCContact?.dateOfBirdth
        companyTextField.text = singleContactVCContact?.company
        
        if let mail = singleContactVCContact?.email {
            if EmailValidator.shared.isValidEmail(mail) {
                emailTextField.text = singleContactVCContact?.email
            } else {
                emailTextField.text = ""
            }
        }
        
        phoneTextField.text = singleContactVCContact?.phone
        if let favourite = singleContactVCContact?.isFavourite {
            isFavouriteSwitch.isOn = favourite
        }
    }
    
    // MARK: - Dismiss keyboard
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    private func setDismissKeyboardGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
}

// MARK: - UITextFieldDelegate Method

extension SingleContactViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let fullString = (textField.text ?? "") + string
        textField.text = PhoneNumberFormater.shared.format(phoneNumber: fullString, shouldRemoveLastDigit: range.length == 1)
        return false
    }
}
