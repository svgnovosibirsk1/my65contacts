//
//  ContactsViewController.swift
//  My65Contacts
//
//  Created by Sergey Golovin on 18.09.2021.
//  Copyright © 2021 GoldenWindGames LLC. All rights reserved.
//

import UIKit
import CoreData

class ContactsViewController: UITableViewController {
    let cellID = "ContactCell"
    let addNewSegueID = "addNewContact"
    let editSegueID = "editContact"
    let toFavouriteSegueID = "toFavourite"
    
    var contacts = [Contact]()
    var contactIndex: IndexPath?
    
    let context = (UIApplication.shared.delegate as!AppDelegate).persistentContainer.viewContext

    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBarTextColor()
        title = "My65Contacts"
        loadContacts()
    }

    @IBAction func favouriteButtonPressed(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: toFavouriteSegueID, sender: self)
    }
    
    // MARK: - Add new contact
    
    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: addNewSegueID, sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == addNewSegueID {
            if let vc = segue.destination as? SingleContactViewController {
                vc.title = "Add new contact"
                vc.delegate = self
            }
        } else if segue.identifier == editSegueID {
            if let vc = segue.destination as? SingleContactViewController {
                vc.title = "Edit contact"
                vc.delegate = self
                if let index = contactIndex {
                    vc.singleContactVCContact = contacts[index.row]
                    vc.index = index
                    vc.isEditMode = true
                }
            }
        }
    }
}

// MARK: - Tableview Datasource Methods

extension ContactsViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        contacts.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
        
        if let lastName = contacts[indexPath.row].surname {
            cell.textLabel?.text = contacts[indexPath.row].name! + " " + lastName
        } else {
            cell.textLabel?.text = contacts[indexPath.row].name
        }
        
        cell.detailTextLabel?.text = contacts[indexPath.row].phone
        cell.imageView?.image = contacts[indexPath.row].isFavourite ? UIImage(systemName: "star.fill") : UIImage(systemName: "star")
        
        return cell
    }
}

// MARK: - Tableview Delegate Methods

extension ContactsViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        contactIndex = indexPath
        performSegue(withIdentifier: editSegueID, sender: self)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - Set NavigationBar TextColor

extension ContactsViewController {
    private func setNavigationBarTextColor() {
        if #available(iOS 13.0, *) {
            navigationController?.navigationBar.standardAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
        } else {
            navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        }
    }
}

// MARK: - AddContact Delegate Methods

extension ContactsViewController: AddContactDelegate {
    func addNewContact(contact: Contact) {
        contacts.append(contact)
        tableView.reloadData()
    }
    
    func editContact(contact: Contact, index: IndexPath) {
        contacts.remove(at: index.row)
        contacts.insert(contact, at: index.row)
        tableView.reloadData()
    }
    
    func deleteContact(at index: IndexPath) {
        context.delete(contacts[index.row])
        contacts.remove(at: index.row)
        saveContacts()
        tableView.reloadData()
    }
}

// MARK: - CoreData methods

extension ContactsViewController {
     func saveContacts() {
         do {
             try context.save()
         } catch {
             print("Saving context error \(error)")
         }
        //tableView.reloadData()
     }
    
    func loadContacts(with request: NSFetchRequest<Contact> = Contact.fetchRequest()) {
        //let request: NSFetchRequest<Contact> = Contact.fetchRequest()
        do {
            contacts = try context.fetch(request)
        } catch {
            print("Fetching data from context error \(error)")
        }
    }
}

// MARK: - UISearchBar Delegate Methods

extension ContactsViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let request: NSFetchRequest<Contact> = Contact.fetchRequest()
        request.predicate = NSPredicate(format: "name CONTAINS[cd] %@ OR surname CONTAINS[cd] %@ OR email CONTAINS[cd] %@ OR phone CONTAINS[cd] %@", searchBar.text!, searchBar.text!, searchBar.text!, searchBar.text!)
        request.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]

        loadContacts(with: request)
        tableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text?.count == 0 {
            loadContacts()
            tableView.reloadData()
            
            DispatchQueue.main.async {
                searchBar.resignFirstResponder()
            }
        }
    }
}
